const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlwebpackPlugin = require('html-webpack-plugin');
const Md5Hash = require('webpack-md5-hash');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname);
const APP_PATH = path.resolve(ROOT_PATH, 'app');
const BUILD_PATH = path.resolve(ROOT_PATH, 'build');

const dependencies = Object.keys(require('./package').dependencies);
dependencies.push(`${APP_PATH}/vendor/dingtalk`);

module.exports = {
  entry: {
    app: path.resolve(APP_PATH, 'index.js'),
    vendors: dependencies,
  },
  output: {
    path: BUILD_PATH,
    filename: 'bundle.[chunkhash:6].js',
    chunkFilename: '[name].[chunkhash:6].js',
  },
  resolve: {
    extensions: ['', '.js'],
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loaders: ['babel'],
        include: APP_PATH,
      },
      {
        test: /\.css$/,
        loaders: [
          'style',
          'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'postcss',
        ],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.eot$|\.woff$|\.ttf$|\.otf$/,
        loader: 'file?name=static/[name].[hash:6].[ext]',
      },
    ],
  },
  postcss() {
    return [autoprefixer];
  },
  plugins: [
    // enable uglify
    new webpack.optimize.UglifyJsPlugin({ minimize: true }),
    // try to fix Vendor chunkhash changes when app code changes
    // https://github.com/webpack/webpack/issues/1315
    // https://gist.github.com/d4goxn/37be7b185cdf4ac28cbb
    new Md5Hash(),
    // split vendors script
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.[chunkhash:6].js'),
    // generate index page
    new HtmlwebpackPlugin({
      template: `${APP_PATH}/index.html`, // Load a custom template
    }),
    new CleanWebpackPlugin(['build'], {
      root: ROOT_PATH,
      verbose: true,
      dry: false,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
    }),
  ],
};
