import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import Todo from '../../../app/components/Todos/Todo.jsx';

describe('Component Todo', function() {
  const todoRecord = {
    content: 'new Todo',
    dueDate: '2015-12-31',
    member: 'zr.png'
  };
  it('should render right el', function() {
    const el = render(
      <Todo content={todoRecord.content} dueDate={todoRecord.dueDate} member={todoRecord.member}></Todo>
    )
    expect(el.find(`div:contains(${todoRecord.content})`).length).to.equal(1);
  })
});