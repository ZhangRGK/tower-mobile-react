const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlwebpackPlugin = require('html-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname);
const APP_PATH = path.resolve(ROOT_PATH, 'app');
const BUILD_PATH = path.resolve(ROOT_PATH, 'build');

const dependencies = Object.keys(require('./package').dependencies);
dependencies.push(`${APP_PATH}/vendor/dingtalk`);

module.exports = {
  entry: {
    app: path.resolve(APP_PATH, 'index.js'),
    vendors: dependencies,
  },
  output: {
    path: BUILD_PATH,
    filename: 'bundle.js',
  },
  // enable dev source map
  devtool: 'eval-source-map',
  // enable dev server
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    port: 3000,
    contentBase: 'build',
  },
  resolve: {
    extensions: ['', '.js'],
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loaders: ['babel'],
        include: APP_PATH,
      },
      {
        test: /\.css$/,
        loaders: [
          'style',
          'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'postcss',
        ],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.eot$|\.woff$|\.ttf$|\.otf$/,
        loader: 'file?name=static/[name].[hash:6].[ext]',
      },
    ],
  },
  postcss() {
    return [autoprefixer];
  },
  plugins: [
    // split vendors script
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
    // generate index page
    new HtmlwebpackPlugin({
      template: `${APP_PATH}/index.html`, // Load a custom template
    }),
  ],
};
