import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../reducers';
import { syncHistory } from 'react-router-redux';
import { browserHistory } from 'react-router';

const logger = store => next => action => {
  console.log('dispatching', action);
  const result = next(action);
  console.log('next state', store.getState());
  return result;
};

export default function configureStore(initialState) {
  // Sync dispatched route actions to the history
  const reduxRouterMiddleware = syncHistory(browserHistory);

  const createStoreWithMiddleware = compose(
    applyMiddleware(reduxRouterMiddleware, logger),
    // devTools
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )(createStore);

  const store = createStoreWithMiddleware(rootReducer, initialState);

  // Required for replaying actions from devtools to work
  reduxRouterMiddleware.listenForReplays(store);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
