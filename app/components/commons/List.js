import React, { Component } from 'react';
import { Link } from 'react-router';
import style from './List.css';

export default class List extends Component {
  render() {
    let ListNodes;
    if(this.props.data) {
      ListNodes = this.props.data.map((line)=> {
        return line?<Line id={line.id} child={line.children} img={line.img} icon={line.icon} desc={line.desc}></Line>:<div className={style.separator}></div>
      });
    }
    return (
      <ul key={1} className={style.list}>
        {ListNodes}
      </ul>
    )
  }
}

class Line extends Component {
  render() {
    let icon;
    if(!!this.props.icon) {
      icon = <i className={this.props.icon}></i>
    } else if(!!this.props.img) {
      icon = <img src={this.props.img} />
    }
    return (
      <li key={this.props.id} className={style.line} data-role="line">
        <Link to={`project/${this.props.id}/${this.props.child}`}>
          {icon}
          {this.props.desc}
        </Link>
      </li>
    )
  }
}
