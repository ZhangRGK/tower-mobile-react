import React, { Component } from 'react';

export default class Toast extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let iconClass;
    switch (this.props.status) {
      case 'error':
        iconClass = 'info';break;
      case 'warning':
        iconClass = 'warning';break;
      case 'success':
        iconClass = 'warning';break;
      default:
        iconClass = 'info';
    }
    return (
      <div><i className={iconClass}></i>{this.props.status}</div>
    )
  }
}
