import React, { Component } from 'react';
import _ from 'lodash';
import Style from './TodoToolbar.css';

export default class TodoToolbar extends Component {
  render() {
    return (
      <div>
        <div className={Style.bg}></div>
        <span>
          <i className="dt dt-ctask-add"></i>
        </span>
      </div>
    )
  }
}