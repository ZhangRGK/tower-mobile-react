import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import Todo from './Todo';
import Style from './TodoList.css';

export default class TodoList extends Component {

  render() {
    const { todos, actions } = this.props
    return (
      <div>
        {this.props.todos.map((todo) => 
          <Todo
            key={todo.id}
            id={todo.id}
            content={todo.content}
            dueDate={todo.dueDate}
            member={todo.member}
            {...actions}
          />
        )}
      </div>
    )
  }
}
