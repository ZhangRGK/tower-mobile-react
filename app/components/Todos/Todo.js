import React, { Component } from 'react';
import _ from 'lodash';

import TodoToolbar from './TodoToolbar';

export default class Todo extends Component {

  render() {
    const { deleteTodo } = this.props;
    return (
      <li>
        <TodoToolbar />
        <div onClick={() => deleteTodo(this.props.id)} >
          {this.props.content}
        </div>
        <div>{this.props.dueDate}</div>
        <div><img src={this.props.member} /></div>
      </li>
    )
  }
}
