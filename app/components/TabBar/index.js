import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import cssModules from 'react-css-modules';
import styles from './index.css';

const TabBar = class TabBar extends Component {

  render() {
    return (
      <ul styleName="tabs">
        <Link to="/">
          <li styleName="tab">任务</li>
        </Link>
        <Link to="/projects">
          <li styleName="tab">项目</li>
        </Link>
        <Link to="/events">
          <li styleName="tab">动态</li>
        </Link>
        <Link to="/profile">
          <li styleName="tab">我</li>
        </Link>
      </ul>
    );
  }
};

export default cssModules(TabBar, styles);
