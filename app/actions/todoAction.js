import * as types from '../constants/ActionTypes';

export function initTodos(todos) {
  return { type: types.INIT_TODO, todos };
}

export function addTodo(content, dueDate = null, member = null) {
  return { type: types.ADD_TODO, content, dueDate, member };
}

export function editTodo(id, content, dueDate = null, member = null) {
  return { type: types.EDIT_TODO, id, content, dueDate, member };
}

export function deleteTodo(id) {
  return { type: types.DELETE_TODO, id };
}
