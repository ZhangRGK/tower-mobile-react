import dd from '../vendor/dingtalk';

export default {
  // 钉钉客户端低于 2.5.0 提示升级
  compatible() {
    if (dd && !dd.compareVersion('2.5.0', dd.version)) {
      dd.device.notification.alert({
        title: '提示',
        message: '请升级到最新版钉钉',
        onSuccess() {
          this.openLink('http://www.dingtalk.com');
        },
      });
    }
  },

  // icon: 'success', 'error'
  toast(text, icon = '') {
    this.loading.hide();
    dd.device.notification.toast({
      text,
      icon,
      duration: 2,
    });
  },

  loading: {
    show(text = '') {
      dd.device.notification.showPreloader({ text });
    },
    hide() {
      dd.device.notification.hidePreloader();
    },
  },

  datepicker(date, callback) {
    dd.biz.util.datepicker({
      format: 'yyyy-MM-dd',
      value: date,
      onSuccess: callback(),
    });
  },

  // 钉钉内新窗口打开页面
  openLink(url) {
    dd.biz.util.openLink({ url });
  },

  navigation: {
    // 设置导航栏标题
    setTitle(title = 'Tower') {
      dd.biz.navigation.setTitle({ title });
    },

    // 设置导航栏右侧按钮文案、点击行为
    setRight(text, callback) {
      dd.biz.navigation.setRight({
        control: true,
        show: true,
        text: '新任务',
        onSuccess: callback(),
      });
    },

    // 设置导航栏图标、点击行为
    // iconIndex 1:问号, 2:问号+NEW 3:问号+HOT
    setIcon(iconIndex, callback) {
      dd.biz.navigation.setIcon({
        iconIndex,
        showIcon: true,
        onSuccess: callback(),
      });
    },
  },

  // Modal 弹窗
  // buttonLabels: 弹窗按钮，格式为 ['查看详情', '知道了']
  modal(image, title, content, buttonLabels, callback) {
    dd.device.notification.extendModal({
      cells: [image, title, content],
      buttonLabels,
      onSuccess: callback(),
    });
  },

  // 返回参数 buttonIndex：被点击按钮的索引值，从 0 开始
  confirm(message, callback) {
    dd.device.notification.confirm({
      message,
      title: '提示',
      buttonLabels: ['确认', '取消'],
      onSuccess: callback(),
    });
  },

  // 下拉控件，source 格式如下：
  // [{
  //   key: '显示文本',
  //   value: '值',
  // }]
  chosen(source, callback) {
    dd.biz.util.chosen({
      source,
      onSuccess: callback(),
    });
  },

  // 企业通讯录选人，从企业成员中选择
  contactChoose(multiple = true, users = [], disabledUsers = [], callback) {
    dd.biz.contact.choose({
      multiple,           // 是否可多选
      users,              // 默认选中成员
      disabledUsers,      // 不可选成员
      corpId: 'corp_id',  // 需要传企业 corp_id
      startWithDepartmentId: 0,
      max: 1500,
      onSuccess: callback(),
    });
  },

  // 自定义选人，可从指定成员中选择
  customContact: {
    choose(title, users, callback) {
      dd.biz.customContact.choose({
        title,              // 选人组件标题
        users,              // 一组成员工号（dingtalk id）
        corpId: 'corp_id',  // 需要传企业 corp_id
        isShowCompanyName: true,
        onSuccess: callback(),
      });
    },

    multipleChoose(title, users, disabledUsers = [], max = 20, callback) {
      dd.biz.customContact.multipleChoose({
        title,          // 选人组件标题
        users,          // 一组成员工号（dingtalk id）
        disabledUsers,  // 不可选成员
        max,            // API 实现有缺陷，如果需要不限制上线，传 1500
        corpId: 'corp_id',  // 需要传企业 corp_id
        onSuccess: callback(),
      });
    },
  },
};
