import fetch from 'fetch'

const headers = new fetch.Header({
  'Content-Type': 'application/vnd.api+json'
});

export default function fetchWrapper (url, opts) {
  return fetch(url, Object.assign({}, opts, {
    headers: headers
  }));
}