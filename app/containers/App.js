import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import { browserHistory, IndexRoute } from 'react-router';
import Router from 'react-router/lib/Router';
import Route from 'react-router/lib/Route';
// containers
import ProjectsPage from './ProjectsPage';
import TodosPage from './TodosPage';
import EventsPage from './EventsPage';
import ProfilePage from './ProfilePage';
// components
import TabBar from '../components/TabBar';

function Root({ children }) {
  return (
    <div>
      { children }
      <TabBar />
    </div>
  );
}

export default class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Router history={browserHistory}>
          <Route path="/" component={Root}>
            <IndexRoute component={TodosPage} />
            <Route path="projects" component={ProjectsPage}/>
            <Route path="events" component={EventsPage}/>
            <Route path="profile" component={ProfilePage}/>
          </Route>
        </Router>
      </Provider>
    );
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired,
};
