import React, { Component } from 'react';

import Event from '../components/Events/Event';

class EventsPage extends Component {
  render() {
    return (
        <Event />
    );
  }
}

export default EventsPage;
