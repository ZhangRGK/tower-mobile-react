import React, { Component } from 'react';

import Profile from '../components/Profile/Profile';

class ProfilePage extends Component {
  render() {
    return (
        <Profile />
    );
  }
}

export default ProfilePage;
