import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as TodoActions from '../actions/todoAction';

import TodoList from '../components/Todos/TodoList';

class TodoPage extends Component {
  render() {
    const { todos, actions } = this.props;
    return (
        <TodoList todos={ todos } actions={ actions } />
    );
  }
}

TodoPage.propTypes = {
  todos: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoPage);
