import React, { Component } from 'react';

import Project from '../components/Projects/Project';

class ProjectsPage extends Component {
  render() {
    return (
        <Project />
    );
  }
}

export default ProjectsPage;
