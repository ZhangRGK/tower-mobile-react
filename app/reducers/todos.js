import createReducer from '../utils/createReducer';
import {
  INIT_TODO,
  ADD_TODO,
  DELETE_TODO,
  EDIT_TODO,
} from '../constants/ActionTypes';

function initTodo(state, action) {
  return action.todos;
}

function addTodo(state, action) {
  return [...state, {
    content: action.content,
    dueDate: action.dueDate,
    member: action.member }];
}

function deleteTodo(state, action) {
  return state.filter(todo =>
      todo.id !== action.id
  );
}

function editTodo(state, action) {
  return state.map(todo =>
    todo.id === action.id ?
      Object.assign({}, todo, { text: action.text }) :
      todo
  );
}

export default createReducer([], {
  [INIT_TODO]: initTodo,
  [ADD_TODO]: addTodo,
  [EDIT_TODO]: editTodo,
  [DELETE_TODO]: deleteTodo,
});
