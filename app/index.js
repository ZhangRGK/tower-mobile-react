import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore';
import App from './containers/App';
// global css
import 'normalize.css';
import './base.css';

window.STATE_FROM_SERVER = {
  todos: [{
    id: 10,
    content: '第一个任务',
    dueDate: null,
    member: 'zr',
  }, {
    id: 11,
    content: '第二个任务',
    dueDate: null,
    member: 'zr',
  }, {
    id: 12,
    content: '第三个任务',
    dueDate: null,
    member: null,
  }],
};

const store = configureStore(window.STATE_FROM_SERVER);

ReactDOM.render(
  <App store={ store }/>,
  document.getElementById('main')
);
