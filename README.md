# tower-mobile-react

## Install

```
git clone git@github.com:mycolorway/tower-mobile-react.git
npm install
```

[redux devtools Chrome Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)

## Run

```
npm run dev
open http://localhost:3000/
```

## Test

```
npm run test
```

## Dependencies

[BABEL-ES2015](https://babeljs.io/docs/learn-es2015/)

[React](https://facebook.github.io/react/)[中文](http://reactjs.cn/)

[React-router](https://github.com/rackt/react-router/tree/latest/docs)[中文](http://react-guide.github.io/react-router-cn/index.html)

[CSS Modules](https://github.com/css-modules/css-modules)

[Redux](https://github.com/rackt/redux)[中文](http://camsong.github.io/redux-in-chinese/index.html)

[ImmutableJS](https://facebook.github.io/immutable-js/docs/#/)
